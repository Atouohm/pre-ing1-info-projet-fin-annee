/* inclusion des headers des bibliothèques classiques */
#include "basicLibraries.h"

/* inclusion de nos headers pour nos bibliothèques */
#include "utilities.h"
#include "basicHandling.h"
#include "convolution.h"
#include "segmentation.h"
#include "autoParsing.h"
#include "bonus.h"

/* fonction main prenant en paramètre les arguments de la commande d'exécution qui sont définis comme constants (pour la sécurité) et donc non modifiables (les cases du tableau argv, pas les chaînes de caractères de argv en elles-mêmes), les cases du tableau statique les contenants y compris */
int main(int argc, char* const argv[]){
	LISTINSTRUCTIONS* listInstructions = NULL; // on déclare la liste d'instructions
	listInstructions = getInstructions(argc, argv); // on la récupère avec les instructions dans l'ordre et vérifiées
	char* target = malloc(128*sizeof(char)); // on déclare et alloue la chaîne représentant l'adresse du fichier cible
	strcpy(target, "./images/croix.ppm"); // on lui met une valeur par défaut pour la fonction croix plus tard
	char* source = malloc(128*sizeof(char)); // on déclare et alloue la chaîne représentant l'adresse du fichier source
	int noOutput = 1; // pour l'instant il n'y a pas de fichier de sortie configuré
	int xFlag = 0; // et l'option -x n'a pas été utilisée

	IMAGE* img = NULL; // on crée l'image
	FILTER* crossFilter = createCrossFilter(3); // on crée un filtre en croix pour l'érosion et la dilatation plus tard

	if(!strcmp(listInstructions->list[0], "h")){ // on regarde si la premier (et unique) instruction est -h
		printHelp(); // si c le cas on affiche l'aide
		stopProg("", 0); // et on quitte le prog sans erreur
	} else{ // sinon on vérifie s'il y a un -o, et un -i ou un -x

		if(!strcmp(listInstructions->list[listInstructions->size-2], "o")){ // on regarde si -o
			strcpy(target, listInstructions->list[listInstructions->size-1]); // on récup le fichier cible
			noOutput = 0; // on vient de configurer une sortie donc on le spécifie en mettant noOutput à 0 (faux)
		}

		if(!strcmp(listInstructions->list[listInstructions->size-2], "i")){ // on regarde si -i
			strcpy(source, listInstructions->list[listInstructions->size-1]); // on récup le fichier source
		} else if(!strcmp(listInstructions->list[listInstructions->size-4], "i")){ // on regarde si -i au deuxième et dernier emplacement possible du -i
			strcpy(source, listInstructions->list[listInstructions->size-3]);
		} else if(!strcmp(listInstructions->list[listInstructions->size-4], "x")){ // on regarde si -x
			FILE* targetFile = fopen(target, "w");
			if(targetFile == NULL){endOfMain("file saving failed", 1, img, crossFilter, listInstructions, target, source);}
			customCross(targetFile, atoi(listInstructions->list[listInstructions->size-3]), atoi(listInstructions->list[listInstructions->size-2]), atoi(listInstructions->list[listInstructions->size-1])); // on lance directement la procédure pour créer une croix, avec les bons arguments
			fclose(targetFile);
			strcpy(source, target); // on met le fichier cible en tant que source
			xFlag = 1; // on dit qu'on a l'option -x qui a été utilisée
		} else{ // il y aura alors forcément un -x au deuxième et dernier emplacement possible du -x
			FILE* targetFile = fopen(target, "w");
			if(targetFile == NULL){endOfMain("file saving failed", 1, img, crossFilter, listInstructions, target, source);}
			customCross(targetFile, atoi(listInstructions->list[listInstructions->size-5]), atoi(listInstructions->list[listInstructions->size-4]), atoi(listInstructions->list[listInstructions->size-3]));
			fclose(targetFile);
			strcpy(source, target);
			xFlag = 1;
		}
		img = loadImage(source); // on charge l'image source
		if(img == NULL){endOfMain("file loading failed", 1, img, crossFilter, listInstructions, target, source);}
	}

	int index = 0; // on met l'index à 0 qui va nous permettre de parcourir la liste d'instructions
	while(strcmp(listInstructions->list[index], "x") && strcmp(listInstructions->list[index], "i")){ // tant qu'on arrive pas à la fin des instructions basiques (ie tout sauf -i, -o, -x)
		switch(listInstructions->list[index][0]){ // on regarde l'instruction à effectuer, et on a tous les cas possibles avec switch, il suffit alors d'appeler les bonnes fonctions/procédure suivant les cas
			case 'a':
				printf("\ncréation de l'histogramme...");
				imgGreyTransform(img);
				int* hist = imgHist(*img);
				if(!saveHistogram(hist, img->colorRange+1)){endOfMain("", 1, img, crossFilter, listInstructions, target, source);}
	 			break;
	 		case 'p':
	 			printf("\nrotation de 90 degrés vers la droite...");
	 			imgRotate(img);
	 			break;
	 		case 'm':
	 			printf("\ncréation du miroir...");
	 			imgMirror(img);
	 			break;
	 		case 'g':
	 			printf("\nmise en niveaux de gris...");
	 			imgGreyTransform(img);
	 			break;
	 		case 'b':
	 			printf("\nbinarisation...");
	 			index++; // on avance pour récupérer l'argument
	 			imgBinar(img, atoi(listInstructions->list[index]));
	 			break;
	 		case 'n':
	 			printf("\ntransformation en négatif...");
	 			imgNegative(img);
	 			break;
	 		case 'r':
	 			printf("\nrecadrage dynamique...");
	 			imgGreyTransform(img);
				dynamicCropping(img);
	 			break;
	 		case 'c':
	 			printf("\nrenforcement de contraste...");
	 			applyContrast(img);
	 			break;
	 		case 'f':
	 			printf("\nflou...");
	 			applyBlurring(img);
	 			break;
	 		case 'I':
	 			printf("\ncontours...");
	 			applyContours(img);
	 			break;
	 		case 'e':
	 			printf("\nérosion...");
	 			imgGreyTransform(img);
				imgErode(img, crossFilter);
	 			break;
	 		case 'd':
	 			printf("\ndilatation...");
	 			imgGreyTransform(img);
				imgDilate(img, crossFilter);
	 			break;
	 		case 'z':
	 			printf("\nrétrécissement de l'image...");
				dezoom(img);
	 			break;
	 		case 'Z':
	 			printf("\nagrandissement de l'image...");
				zoom(img);
	 			break;
	 		default:
	 			endOfMain("big issue", 1, img, crossFilter, listInstructions, target, source);
	 			break;
		}
		index++; // on avance pour voir la prochaine option
	}

	if(xFlag){ // si un -x a été utilisé
		if(!saveImage(*img, target)){endOfMain("file saving failed", 1, img, crossFilter, listInstructions, target, source);} // on sauvegarde exceptionnellement l'image, même si noOutput est à 1
	} else if(noOutput){ // si noOutput = 0 (ie faux), alors on affiche l'image dans la console
		printImage(*img);
	} else{ // sinon on a bien un fichier cible, et on enregistre l'image dedans
		if(!saveImage(*img, target)){endOfMain("file saving failed", 1, img, crossFilter, listInstructions, target, source);}
	}

	endOfMain("", 0, img, crossFilter, listInstructions, target, source); // on termine la fonction et le programme proprement
	
	return 0;
}
	
	// (à remplacer dans le main) pour les tests et passer outre les commandes
	/*IMAGE* img;
	FILTER* crossFilter;
	crossFilter = createCrossFilter(3);
	
	img = loadImage("./images/bender.ppm");
	imgGreyTransform(img);
	saveImage(*img, "./images/imgGrey.ppm");

	img = loadImage("./images/bender.ppm");
	imgBinar(img, 170);
	saveImage(*img, "./images/imgBinar.ppm");

	img = loadImage("./images/bender.ppm");
	imgMirror(img);
	saveImage(*img, "./images/imgMirror.ppm");

	img = loadImage("./images/bender.ppm");
	imgRotate(img);
	saveImage(*img, "./images/imgRotate.ppm");

	img = loadImage("./images/bender.ppm");
	imgNegative(img);
	saveImage(*img, "./images/imgNegative.ppm");

	img = loadImage("./images/bender.ppm");
	imgGreyTransform(img);
	dynamicCropping(img);
	saveImage(*img, "./images/imgCropping.ppm");

	img = loadImage("./images/bender.ppm");
	applyContrast(img);
	saveImage(*img, "./images/imgContrast.ppm");

	img = loadImage("./images/bender.ppm");
	applyBlurring(img);
	saveImage(*img, "./images/imgBlur.ppm");

	img = loadImage("./images/bender.ppm");
	applyContours(img);
	saveImage(*img, "./images/imgContours.ppm");

	img = loadImage("./images/bender.ppm");
	imgGreyTransform(img);
	imgErode(img, crossFilter);
	saveImage(*img, "./images/imgErode.ppm");

	img = loadImage("./images/bender.ppm");
	imgGreyTransform(img);
	imgDilate(img, crossFilter);
	saveImage(*img, "./images/imgDilate.ppm");

	FILE* targetFile = fopen("./images/cross.ppm", "w");
	customCross(targetFile, 50, 25, 5);
	fclose(targetFile);

	img = loadImage("./images/bender.ppm");
	imgGreyTransform(img);
	int* hist = imgHist(*img);
	saveHistogram(hist, img->colorRange+1);*/