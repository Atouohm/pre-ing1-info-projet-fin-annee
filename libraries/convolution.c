#include <convolution.h>

/*
	Auteur : Dorian BARRERE
	Date : 01/06/2022
	Résumé : fonction qui étend une image d'un certain nombre de pixels (à la fois sur la hauteur, la largeur et les coins) pour appliquer plus tard la convolution
	Entrée(s) : la structure file (du fichier), un entier pour le nombre de pixels à étendre sur les côtés
	Sortie(s) : le tableau dynamique entendu de pixels
*/
PIXEL** createExtendedTabPixels(IMAGE img, int range){
	PIXEL** newTabPixels = createTabPixels(img.height+2*range, img.width+2*range); // on crée le nouveau tableau de pixels qui sera la version étendue, d'où l'ajout de 2*range
	int line, column, index; // on déclare les variables pour les boucles puisqu'elles sont utilisées dans plusieurs boucles, index permet de se déplacer dans l'image et de régler les points de départ

	/* 1 : on affecte aux bonnes valeurs les lignes étendues à la fois au dessus et en dessous de l'image */
	for(index=0 ; index<range ; index++){ // on récupère l'indice des lignes étendues
		for(column=0 ; column<img.width; column++){ // on récupère les colonnes des lignes
			newTabPixels[index][range+column].red = img.tabPixels[0][column].red; // pour les lignes au dessus
			newTabPixels[index][range+column].green = img.tabPixels[0][column].green;
			newTabPixels[index][range+column].blue = img.tabPixels[0][column].blue;

			newTabPixels[img.height+range+index][range+column].red = img.tabPixels[img.height-1][column].red; // pour les lignes en dessous
			newTabPixels[img.height+range+index][range+column].green = img.tabPixels[img.height-1][column].green;
			newTabPixels[img.height+range+index][range+column].blue = img.tabPixels[img.height-1][column].blue;
		}
	}

	/* 2 : on affecte aux bonnes valeurs les colonnes étendues à la fois à gauche et à droite de l'image */
	for(index=0 ; index<range ; index++){ // on récupère l'indice des colonnes étendues
		for(line=0 ; line<img.height; line++){ // on récupère les lignes des colonnes
			newTabPixels[range+line][index].red = img.tabPixels[line][0].red; // pour les colonnes à gauche
			newTabPixels[range+line][index].green = img.tabPixels[line][0].green;
			newTabPixels[range+line][index].blue = img.tabPixels[line][0].blue;

			newTabPixels[range+line][img.width+range+index].red = img.tabPixels[line][img.width-1].red; // pour les colonnes à droite
			newTabPixels[range+line][img.width+range+index].green = img.tabPixels[line][img.width-1].green;
			newTabPixels[range+line][img.width+range+index].blue = img.tabPixels[line][img.width-1].blue;
		}
	}

	/* 3 : on affecte aux bonnes valeurs les pixels se trouvant sur les coins étendus */
	for(index=0 ; index<=img.width+range ; index=index+img.width+range){ // on gère les 2 coins à gauche puis les 2 à droite
		for(line=0 ; line<range ; line++){ // on récupère l'indice des lignes étendues
			for(column=0 ; column<range; column++){ // on récupère l'indice des colonnes étendues
				newTabPixels[line][column+index].red = newTabPixels[range][index].red; // pour le coin supérieur
				newTabPixels[line][column+index].green = newTabPixels[range][index].green;
				newTabPixels[line][column+index].blue = newTabPixels[range][index].blue;

				newTabPixels[img.height+range+line][column+index].red = newTabPixels[img.height+range-1][index].red; // pour le coin inférieur
				newTabPixels[img.height+range+line][column+index].green = newTabPixels[img.height+range-1][index].green;
				newTabPixels[img.height+range+line][column+index].blue = newTabPixels[img.height+range-1][index].blue;
			}
		}
	}

	/* 4 : on affecte aux bonnes valeurs les pixels de la partie centrale non étendue */
	for(int line=0 ; line<img.height ; line++){ // on parcours les lignes
		for(int column=0 ; column<img.width ; column++){ // on parcours les colonnes
			newTabPixels[line+range][column+range].red = img.tabPixels[line][column].red;
			newTabPixels[line+range][column+range].green = img.tabPixels[line][column].green;
			newTabPixels[line+range][column+range].blue = img.tabPixels[line][column].blue;
		}
	}

	return newTabPixels; // on retourne la nouveau tableau de pixels de l'image étendue
}

void applyConvolution(IMAGE* img, float matrix[3][3]){
	float accumulatorRed, accumulatorGreen, accumulatorBlue;
	PIXEL** newTabPixels = createExtendedTabPixels(*img, 1); // on crée le nouveau tableau de pixel qui sera celui sur lequel on va baser la convolution, on l'étend pour gérer les bords
	
	/* on gère le coefficient pour normaliser la matrice de convolution (ie que la somme des éléments de cette dernière vaut 1) */
	float normalizeCoef = (matrix[0][0] + matrix[0][1] + matrix[0][2] + matrix[1][0] + matrix[1][1] + matrix[1][2] + matrix[2][0] + matrix[2][1] + matrix[2][2]);
	if(normalizeCoef == 0){normalizeCoef = 1;} else {normalizeCoef = 1/normalizeCoef;} // on fait attention au cas où la somme est initialement nulle
	
	for(int line=0 ; line<img->height ; line++){ // on parcours les lignes
		for(int column=0 ; column<img->width ; column++){ // on parcours les colonnes

			accumulatorRed = 0; // à chaque nouvelle évaluation d'un pixel on remet l'accumulateur du canal rouge à 0
			accumulatorGreen = 0; // pareil pour le vert
			accumulatorBlue = 0; // pareil pour le bleu
			for(int cooX=0 ; cooX<3 ; cooX++){ // on parcours toutes les lignes de la matrice de convolution 3x3
				for(int cooY=0 ; cooY<3 ; cooY++){ // on parcours toutes les colonnes de la matrice de convolution 3x3
					accumulatorRed = accumulatorRed + newTabPixels[line+1+-1+cooX][column+1-1+cooY].red*matrix[cooX][cooY]; // c'est ligne+range et column+range mais ici la matrice est toujours de 3x3 donc la range et toujours de 1
					accumulatorGreen = accumulatorGreen + newTabPixels[line+1-1+cooX][column+1-1+cooY].green*matrix[cooX][cooY];
					accumulatorBlue = accumulatorBlue + newTabPixels[line+1-1+cooX][column+1-1+cooY].blue*matrix[cooX][cooY];
				}
			}

			/* on affecte les valeurs trouvées par les accumulateurs à notre tableau de pixels de l'image */
			img->tabPixels[line][column].red = keepColorRange(accumulatorRed*normalizeCoef, img->colorRange);
			img->tabPixels[line][column].green = keepColorRange(accumulatorGreen*normalizeCoef, img->colorRange);
			img->tabPixels[line][column].blue = keepColorRange(accumulatorBlue*normalizeCoef, img->colorRange);
		}
	}

	freeTabPixels(newTabPixels, img->height+2*1); // on libère le tableau étendu de img->height+2*range lignes, or ici comme la matrice est toujours de 3x3, la range et toujours de 1
}

void applyContrast(IMAGE* img){
	float matrixContrast[3][3] = {{0, -1, 0}, {-1, 5, -1}, {0, -1, 0}}; // on crée la matrice de contraste
	applyConvolution(img, matrixContrast); // on l'applique à l'image
}

void applyBlurring(IMAGE* img){
	float matrixBlurring[3][3] = {{0.0625, 0.125, 0.0625}, {0.125, 0.25, 0.125}, {0.0625, 0.125, 0.0625}}; // on crée la matrice de flou
	applyConvolution(img, matrixBlurring); // on l'applique à l'image
}

void applyContours(IMAGE* img){
	float matrixContours[3][3] = {{-1, -1, -1}, {-1, 8, -1}, {-1, -1, -1}}; // on crée la matrice de contours
	applyConvolution(img, matrixContours); // on l'applique à l'image
}
