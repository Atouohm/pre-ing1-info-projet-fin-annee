#include <autoParsing.h>

/*
	Auteur : Dorian BARRERE
	Date : 03/05/2022
	Résumé : procédure qui automatise la fin prématurée de la fonction getInstructions()
	Entrée(s) : l'adresse de la structure listInstructions, l'adresse des 6 chaînes de caractères (l'ordre importe en réalité peu)
	Sortie(s) : rien
*/
void endOfGetInstructions(LISTINSTRUCTIONS* listInstructions, char* iValue, char* oValue, char* xValue, char* yValue, char* thicknessValue, char* bValue){
	/* on enchaîne simplement les free */
	free(listInstructions->list);
	free(listInstructions);
	free(iValue);
	free(oValue);
	free(xValue);
	free(yValue);
	free(thicknessValue);
	free(bValue);
	printHelp(); // on affiche l'aide en appelant la procédure
	stopProg("error in the command", 1); // on arrête le programme
}

/*
	Auteur : Dorian BARRERE
	Date : 03/05/2022
	Résumé : fonction qui retourne l'adresse structure listInstructions contenant les instructions ordonnées (suivant toutes les règles du sujet) à effectuer dans l'ordre, cela en mettant simplement en paramètre le argv du main
	Entrée(s) : le argc et le argv du main
	Sortie(s) : l'adresse de la structure listInstructions
*/
LISTINSTRUCTIONS* getInstructions(int argc, char* const argv[]){
	/* avant tout, si il n'y a aucun argument/option, cela ne sert à rien de continuer, on affiche directement l'aide*/
	if(argc == 1){
		printHelp();
		stopProg("error, a -i or -x must be used at least", 1);
	}

	/* on crée et on alloue notre liste d'instruction que l'on va retourner à la fin */
	LISTINSTRUCTIONS* listInstructions = malloc(sizeof(LISTINSTRUCTIONS));
	listInstructions->size = 0; // pour l'instant aucune instruction
	listInstructions->list = malloc(sizeof(char*)); // mais on l'alloue quand même, même si "vide"

	/* on récup et on formate comme ça nous arrange l'argv (chaque option '-' séparés par '\n', chaque argument de l'option séparé par ' ') */
	char argsList[320] = ""; // on crée la chaîne de caractères qui va lier tout le argv
	strcpy(argsList, argv[1]); // on récupère la première case de argv
	for(int i=2 ; i<argc ; i++){ // on récupère toutes les autres
		if(strlen(argsList)+strlen(argv[i])+1>320){ // on vérifie que la commande n'est pas trop longue pour la chaîne
			printHelp();
			stopProg("the command line is too long", 1);
		} else if(argv[i][0] == '-'){ // s'il ya un tiret, on met un line feed pour plus tard, c une façon de formater pour que la manipulation soit plus simple après
			argsList[strlen(argsList)] = '\n';
		} else{ // sinon un simple espace suffit
			argsList[strlen(argsList)] = ' ';
		}
		strcat(argsList, argv[i]); // on "ajoute" la suite de argv à la suite de notre chaîne de caractères
	}
	argsList[strlen(argsList)] = '\n'; // pour la fin on met aussi un '\n'
	argsList[strlen(argsList)] = '\0'; // on comme c'est une chaîne on met aussi le '\0' que l'on a écrasé en réalité à la ligne d'avant

	/* on génère la liste précise d'intructions en parcourant toute la liste des opts et args formatés dans argsList, en respectant toutes les régles */
	/* MAIS avant tout on déclare toutes les variables et on alloue les chaînes */
	int hFlag = 0; // -h aide
	int iFlag = 0; // -i <fichier> fichier d'entrée (obligatoire sauf si -h OU -x où ya pas)
	char* iValue = malloc(128*sizeof(char));
	int oFlag = 0; // -o <fichier> fichier de sortie
	char* oValue = malloc(128*sizeof(char));
	int xFlag = 0; // -x <largeur> <hauteur> <épaisseur> croix
	char* xValue = malloc(8*sizeof(char));
  	char* yValue = malloc(8*sizeof(char));
  	char* thicknessValue = malloc(8*sizeof(char));
	int aFlag = 0; // -a histogramme
	int pFlag = 0; // -p rotation
	int mFlag = 0; // -m miroir
	int gFlag = 0; // -g niveau de gris
 	int bFlag = 0; // -b <seuil> binarisation
 	char* bValue = malloc(8*sizeof(char));
 	int nFlag = 0; // -n négatif
 	int rFlag = 0; // -r recadrage dynamique
 	int cFlag = 0; // -c renforcement de contraste
 	int fFlag = 0; // -f flou
 	int IFlag = 0; // -I contours
 	int eFlag = 0; // -e érosion
 	int dFlag = 0; // -d dilatation
 	int zFlag = 0; // -z dezoom
 	int ZFlag = 0; // -Z zoom
 	
 	int index1 = 0; // il va nous permettre de suivre les caractères de argslist
 	int index2; // un index plus temporaire qui va nous permettre de recopier des parties de la grande chaîne
 	
 	/* la grande boucle qui fait tout */
 	while(index1<strlen(argsList)){

 		if(argsList[index1]=='-'){ // si on est bien en face d'une option
 			index1++; // on avance dans argslist pour voir quel option c'est

 			while(argsList[index1]!=' ' && argsList[index1]!='\n'){ // le temps qu'on est toujours dans "le même -" on lit tout, notamment s'il y a plusieurs options rattachées
	 			switch (argsList[index1]){ // le switch (qui prend toute la fonction) de l'option actuellement récupérée et à étudier pour voir si elle est ok
	 				case 'h': // si -h
	 					if(hFlag==1 || index1!=1){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);} // si -h n'est pas au tout début ou s'il y a un double -h, alors erreur et on arrête le prog
	 					hFlag = 1; // on note qu'on a rencontré un -h
	 					break;
	 				case 'i': // si -i
	 					index1++; // on avance dans argslist
	 					if(iFlag==1 || argsList[index1]!=' '){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);} // si ya déjà un -i qui avant était appelé, ou s'il n'y a pas la disposition pour un argument à la suite de cette option, alors erreur et on arrête le prog
	 					index1++; // on avance dans argslist
	 					index2 = 0; // on commence à construite notre chaîne qui contiendra la valeur de l'argument en démarrant à 0
	 					while(argsList[index1]!=' ' && argsList[index1]!='\n'){ // on récupère l'argument
	 						if(index2>127){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);} // on vérifie qu'il n'est pas trop long
	 						iValue[index2] = argsList[index1]; // on affecte caractère par caractère à notre chaîne qui contiendra la valeur de l'argument
	 						index1++; // on avance
	 						index2++; // on avance
	 					}
	 					iValue[index2] = '\0'; // c'est une chaîne faite caractère par caractère donc on n'oublie pas l'indication de fin
	 					if(argsList[index1]==' '){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);} // s'il y a un argument en plus, il est de trop donc erreur
	 					index1--; // on revient en arrière car dans la plupart des autres cas, on ne va pas aussi loin
	 					iFlag = 1; // on note qu'on a rencontré un -i
	 					break;
	 				case 'o': // si -o, exactement de la même façon que -i
	 					index1++;
	 					if(oFlag==1 || argsList[index1]!=' '){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 					index1++;
	 					index2 = 0;
	 					while(argsList[index1]!=' ' && argsList[index1]!='\n'){
	 						if(index2>127){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 						oValue[index2] = argsList[index1];
	 						index1++;
	 						index2++;
	 					}
	 					oValue[index2] = '\0'; // c'est une chaîne faite caractère par caractère donc on n'oublie pas l'indication de fin
	 					if(argsList[index1]==' '){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 					index1--;
	 					oFlag = 1;
	 					break;
	 				case 'x': // si -x, exactement de la même façon que -o et -i, avec juste ici le cas plus long car 3 arguments
	 					index1++;
	 					if(xFlag==1 || argsList[index1]!=' '){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 					index1++;
	 					index2 = 0;
	 					while(argsList[index1]!=' ' && argsList[index1]!='\n'){
	 						if(argsList[index1]<48 || argsList[index1]>57 || index2>7){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);} // on vérifie que c'est bien un nombre et qu'il n'est pas trop grand
	 						xValue[index2] = argsList[index1]; // on le révupère dans xValue
	 						index1++;
	 						index2++;
	 					}
	 					xValue[index2] = '\0'; // c'est une chaîne faite caractère par caractère donc on n'oublie pas l'indication de fin
	 					if(argsList[index1]!=' '){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 					index1++;
	 					index2 = 0;
	 					while(argsList[index1]!=' ' && argsList[index1]!='\n'){
	 						if(argsList[index1]<48 || argsList[index1]>57 || index2>7){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 						yValue[index2] = argsList[index1];
	 						index1++;
	 						index2++;
	 					}
	 					yValue[index2] = '\0'; // c'est une chaîne faite caractère par caractère donc on n'oublie pas l'indication de fin
	 					if(argsList[index1]!=' '){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 					index1++;
	 					index2 = 0;
	 					while(argsList[index1]!=' ' && argsList[index1]!='\n'){
	 						if(argsList[index1]<48 || argsList[index1]>57 || index2>7){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 						thicknessValue[index2] = argsList[index1];
	 						index1++;
	 						index2++;
	 					}
	 					thicknessValue[index2] = '\0'; // c'est une chaîne faite caractère par caractère donc on n'oublie pas l'indication de fin
	 					if(argsList[index1]==' '){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 					index1--;
	 					xFlag = 1; // on note qu'on a rencontré un -x
	 					break;
	 				case 'a': // si -a
	 					if(aFlag==1){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);} // si on en avait déjà rencontré 1, alors erreur
	 					aFlag = 1; // sinon on note qu'on a rencontré un, le premier
	 					listInstructions->size++; // on augmente la taille de notre liste d'instructions
	 					listInstructions->list = realloc(listInstructions->list, listInstructions->size*sizeof(char*)); // on la réalloue pour ajouter de la place allouée
	 					listInstructions->list[listInstructions->size-1] = "a"; // on donne la valeur correspondante à l'option
	 					break;
	 				case 'p': // si -p, exactement de la même façon que -a
	 					if(pFlag==1){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 					pFlag = 1;
	 					listInstructions->size++;
	 					listInstructions->list = realloc(listInstructions->list, listInstructions->size*sizeof(char*));
	 					listInstructions->list[listInstructions->size-1] = "p";
	 					break;
	 				case 'm': // si -m, exactement de la même façon que -a
	 					if(mFlag==1){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 					mFlag = 1;
	 					listInstructions->size++;
	 					listInstructions->list = realloc(listInstructions->list, listInstructions->size*sizeof(char*));
	 					listInstructions->list[listInstructions->size-1] = "m";
	 					break;
	 				case 'g': // si -g, exactement de la même façon que -a
	 					if(gFlag==1){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 					gFlag = 1;
	 					listInstructions->size++;
	 					listInstructions->list = realloc(listInstructions->list, listInstructions->size*sizeof(char*));
	 					listInstructions->list[listInstructions->size-1] = "g";
	 					break;
	 				case 'b': // si -b, exactement de la même façon que -x, avec ici le cas plus simple où 1 seul argument
	 					index1++;
	 					if(bFlag==1 || argsList[index1]!=' '){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 					index1++;
	 					index2 = 0;
	 					while(argsList[index1]!=' ' && argsList[index1]!='\n'){
	 						if(argsList[index1]<48 || argsList[index1]>57 || index2>7){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 						bValue[index2] = argsList[index1];
	 						index1++;
	 						index2++;
	 					}
	 					bValue[index2] = '\0'; // c'est une chaîne faite caractère par caractère donc on n'oublie pas l'indication de fin
	 					if(argsList[index1]==' '){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 					index1--;
	 					bFlag = 1;
	 					listInstructions->size+=2;
	 					listInstructions->list = realloc(listInstructions->list, listInstructions->size*sizeof(char*));
	 					listInstructions->list[listInstructions->size-2] = "b";
	 					listInstructions->list[listInstructions->size-1] = bValue;
	 					break;
	 					break;
	 				case 'n': // si -n, exactement de la même façon que -a
	 					if(nFlag==1){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 					nFlag = 1;
	 					listInstructions->size++;
	 					listInstructions->list = realloc(listInstructions->list, listInstructions->size*sizeof(char*));
	 					listInstructions->list[listInstructions->size-1] = "n";
	 					break;
	 				case 'r': // si -r, exactement de la même façon que -a
	 					if(rFlag==1){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 					rFlag = 1;
	 					listInstructions->size++;
	 					listInstructions->list = realloc(listInstructions->list, listInstructions->size*sizeof(char*));
	 					listInstructions->list[listInstructions->size-1] = "r";
	 					break;
	 				case 'c': // si -c, exactement de la même façon que -a
	 					if(cFlag==1){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 					cFlag = 1;
	 					listInstructions->size++;
	 					listInstructions->list = realloc(listInstructions->list, listInstructions->size*sizeof(char*));
	 					listInstructions->list[listInstructions->size-1] = "c";
	 					break;
	 				case 'f': // si -f, exactement de la même façon que -a
	 					if(fFlag==1){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 					fFlag = 1;
	 					listInstructions->size++;
	 					listInstructions->list = realloc(listInstructions->list, listInstructions->size*sizeof(char*));
	 					listInstructions->list[listInstructions->size-1] = "f";
	 					break;
	 				case 'I': // si -I, exactement de la même façon que -a
	 					if(IFlag==1){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 					IFlag = 1;
	 					listInstructions->size++;
	 					listInstructions->list = realloc(listInstructions->list, listInstructions->size*sizeof(char*));
	 					listInstructions->list[listInstructions->size-1] = "I";
	 					break;
	 				case 'e': // si -e, exactement de la même façon que -a
	 					if(eFlag==1){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 					eFlag = 1;
	 					listInstructions->size++;
	 					listInstructions->list = realloc(listInstructions->list, listInstructions->size*sizeof(char*));
	 					listInstructions->list[listInstructions->size-1] = "e";
	 					break;
	 				case 'd': // si -d, exactement de la même façon que -a
	 					if(dFlag==1){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 					dFlag = 1;
	 					listInstructions->size++;
	 					listInstructions->list = realloc(listInstructions->list, listInstructions->size*sizeof(char*));
	 					listInstructions->list[listInstructions->size-1] = "d";
	 					break;
	 				case 'z': // si -z, exactement de la même façon que -a
	 					if(zFlag==1){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 					zFlag = 1;
	 					listInstructions->size++;
	 					listInstructions->list = realloc(listInstructions->list, listInstructions->size*sizeof(char*));
	 					listInstructions->list[listInstructions->size-1] = "z";
	 					break;
	 				case 'Z': // si -Z, exactement de la même façon que -a
	 					if(ZFlag==1){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);}
	 					ZFlag = 1;
	 					listInstructions->size++;
	 					listInstructions->list = realloc(listInstructions->list, listInstructions->size*sizeof(char*));
	 					listInstructions->list[listInstructions->size-1] = "Z";
	 					break;
	 				default: // par défaut si aucun des cas au dessus
	 					endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue); // c'est qu'il y a un soucis car aucune option n'a été reconnue, donc erreur
	 					break;
	 			}
	 			index1++;
	 		}

 		} else if(argsList[index1]!='\n'){endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);} // sinon si c pas une option cela doit forcément être un '\n' qui correspond à la fin d'une option, sinon c'est qu'il y a une erreur
 		index1++; // on continue d'incrémenter le index1 pour avancer dans argslist (= la liste formatée de caractères représentant argv)
	}

	/* les quelques conditions après la boucle pour gérer les cas particuliers/délicats */
	if(hFlag==1){ // si l'utilisateur a demandé l'aide
		if(listInstructions->size>0){ // s'il a mis d'autres options en même temps
			endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue); // on lui dit qu'il faut pas
		} else{ // sinon c ok, on donne à notre liste d'instruction une unique instruction pour l'aide
			listInstructions->size++;
	 		listInstructions->list = realloc(listInstructions->list, listInstructions->size*sizeof(char*));
	 		listInstructions->list[listInstructions->size-1] = "h";
		}
	} else if((xFlag == 1 && iFlag == 1) || (xFlag == 0 && iFlag == 0)){ // si il y a à la fois -x et -i OU aucun des 2 c'est qu'il y a un problème
		endOfGetInstructions(listInstructions, iValue, oValue, xValue, yValue, thicknessValue, bValue);
	} else if(iFlag == 1){ // si que -i on rajoute l'instruction à la fin de la liste pour toujours savoir sa position quand on interprétera la liste plus tard
		free(xValue);  // on peut free les arguments de l'option -x qui n'y est ici pas, forcément
		free(yValue);
		free(thicknessValue);
		listInstructions->size+=2;
	 	listInstructions->list = realloc(listInstructions->list, listInstructions->size*sizeof(char*));
	 	listInstructions->list[listInstructions->size-2] = "i"; // on met "l'identifiant" de l'option en premier
	 	listInstructions->list[listInstructions->size-1] = iValue; // et son argument après
	} else if(xFlag == 1){ // si que -x on fait pareil mais avec -x
		free(iValue); // même principe que juste avant
		listInstructions->size+=4;
	 	listInstructions->list = realloc(listInstructions->list, listInstructions->size*sizeof(char*));
	 	listInstructions->list[listInstructions->size-4] = "x";
	 	listInstructions->list[listInstructions->size-3] = xValue;
	 	listInstructions->list[listInstructions->size-2] = yValue;
	 	listInstructions->list[listInstructions->size-1] = thicknessValue;
	}

	if(oFlag == 1){ // si on nous donner un fichier de sortie précis, on le met en dernière instruction, cela sera toujours le cas, le -x OU -i suivra juste derrière en avant dernier
		listInstructions->size+=2;
	 	listInstructions->list = realloc(listInstructions->list, listInstructions->size*sizeof(char*));
	 	listInstructions->list[listInstructions->size-2] = "o";
	 	listInstructions->list[listInstructions->size-1] = oValue;
	}

	if(bFlag==0){free(bValue);}
	return listInstructions; // on retourne la liste construite après quelques lignes de code...
}