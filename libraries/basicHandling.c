#include <basicHandling.h>

void imgGreyTransform(IMAGE* img){
	float luminanceGrey = 0;

	// parcours de chaque pixel de l'image
	for(int line = 0 ; line < img->height ; line++){
		for(int column = 0 ; column < img->width ; column++){

			// pour chaque pixel, calcul de la luminance Gris, utilisation de round pour arrondir à l'entier le plus proche
			luminanceGrey = (int)round(img->tabPixels[line][column].red * 0.2126 + img->tabPixels[line][column].green * 0.7152 + img->tabPixels[line][column].blue * 0.0722);

			// pour obtenir une nuance de gris, chaque pixel à ses 3 composantes RGB qui sont de mêmes valeurs = luminance calculée ci dessus
			img->tabPixels[line][column].red = luminanceGrey;
			img->tabPixels[line][column].green = luminanceGrey;
			img->tabPixels[line][column].blue = luminanceGrey;
		}
	}
}

void imgBinar(IMAGE* img, int seuil){
	int luminanceGrey = 0;

	// parcours de tous les pixels de l'image
	for(int line = 0 ; line < img->height ; line++){
		for(int column = 0 ; column < img->width ; column++){

			// pour chaque pixel, calcul de la luminance Gris, utilisation de round pour arrondir à l'entier le plus proche
			luminanceGrey = (int)round(img->tabPixels[line][column].red * 0.2126 + img->tabPixels[line][column].green * 0.7152 + img->tabPixels[line][column].blue * 0.0722);

			if(luminanceGrey > seuil){
				// pixel de couleur blanche
				img->tabPixels[line][column].red = img->colorRange;
				img->tabPixels[line][column].green = img->colorRange;
				img->tabPixels[line][column].blue = img->colorRange;
			}else {
				// pixel de couleur noire
				img->tabPixels[line][column].red = 0;
				img->tabPixels[line][column].green = 0;
				img->tabPixels[line][column].blue = 0;
			}
		}
	}
}

void imgMirror(IMAGE* img){
	PIXEL pixelTmp; // variable temporaire de type structuré PIXEL

	/* On va intervertir deux à deux les pixels d'une même ligne en partant des deux extrémités,
	il suffit donc de balayer la moitié des colonnes seulement */
	for(int line = 0 ; line < img->height ; line++){ // parcours de la moitié gauche des colonnes
		for (int column = 0 ; column < img->width/2 ; column++){
			pixelTmp = img->tabPixels[line][column]; // on stocke un pixel de la moitié gauche de l'image
			img->tabPixels[line][column] = img->tabPixels[line][img->width-column-1]; // le pixel de la partie gauche de l'image prend la valeure de son symétrique de la partie droite de la ligne
			img->tabPixels[line][img->width-column-1] = pixelTmp; //le symétrique prend la valeure initiale du pixel de la partie gauche de la ligne
		}
	}
}

void imgRotate(IMAGE* img){
	PIXEL** newTabPixels = createTabPixels(img->width, img->height);

	for(int line=0 ; line<img->height ; line++){
		for(int column=0 ; column<img->width ; column++){
			newTabPixels[column][img->height-1-line] = img->tabPixels[line][column];
		}
	}

	freeTabPixels(img->tabPixels, img->height); // on libère l'ancien tableau de pixels de l'image qui était celui de base, non modifié
	
	int temp;
	temp = img->height;
	img->height = img->width;
	img->width = temp;

	img->tabPixels = newTabPixels; // on lui donne le nouveau
}

void imgNegative(IMAGE* img){

	// parcours de tous les pixels de l'image
	for(int line = 0 ; line < img->height ; line++){
		for(int column = 0 ; column < img->width ; column++){

			// chaque composante du pixel prend comme nouvelle valeur la luminanceMax (=colorRange) - sa valeur actuelle
			img->tabPixels[line][column].red = img->colorRange-img->tabPixels[line][column].red;
			img->tabPixels[line][column].green = img->colorRange-img->tabPixels[line][column].green;
			img->tabPixels[line][column].blue = img->colorRange-img->tabPixels[line][column].blue;
		}
	}
}

/* 
	Auteur : Dorian BARRERE
	Date : 27/05/2022
	Résumé : fonction qui renvoie la valeur de luminance la plus présente dans un histogramme
	Entrée(s) : l'adresse du tableau d'entiers de l'histogramme, et un entier pour la taille de l'histogramme (du tableau)
	Sortie(s) : un entier, la valeur de luminance la plus présente dans un histogramme
*/
int maxLuminance(int* hist, int taille){
	int max = 0;
	for(int i = 0 ; i<taille ; i++){
		if(hist[i]>hist[max]){
			max = i;
		}
	}
	return max;
}

/* 
	Auteur : Dorian BARRERE
	Date : 27/05/2022
	Résumé : fonction qui renvoie la valeur de luminance la moins présente dans un histogramme
	Entrée(s) : l'adresse du tableau d'entiers de l'histogramme, et un entier pour la taille de l'histogramme (du tableau)
	Sortie(s) : un entier, la valeur de luminance la moins présente dans un histogramme
*/
int minLuminance(int* hist, int taille){
	int min = taille-1;
	for(int i = 0 ; i<taille ; i++){
		if(hist[i]<hist[min]){
			min = i;
		}
	}
	return min;
}

void dynamicCropping(IMAGE* img){
	int newLuminance; // on déclare la variable pour la nouvelle luminance

	int* hist; // on déclare l'histogramme
	hist = imgHist(*img); // on récupère l'histogramme de l'image

	int max = maxLuminance(hist, img->colorRange+1); // on récupère la luminance maximale
	int min = minLuminance(hist, img->colorRange+1); // on récupère la luminance minimale

	for (int line = 0; line < img->height; line++) { // on parcours toutes les lignes de l'image
		for (int column = 0; column < img->width; column++) { // on parcours toutes les colonnes de l'image

			newLuminance = keepColorRange((img->tabPixels[line][column].red - min)*(img->colorRange/(float)(max-min)), img->colorRange); // on calcule la nouvelle luminance

			/* on l'affecte aux 3 canaux */
			img->tabPixels[line][column].red = newLuminance;
			img->tabPixels[line][column].green = newLuminance;
			img->tabPixels[line][column].blue = newLuminance;
		}
	}
}