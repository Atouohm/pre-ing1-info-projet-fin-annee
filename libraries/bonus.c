#include <bonus.h>

int saveHistogram(int* hist, int size){
	FILE* file = fopen("./histogram/hist.dat", "w"); // on ouvre ou crée simplement le fichier data qui stockera en 2 colonne les x et y du tableau en 2D
	if(file == NULL){ // gestion de l'erreur si l'ouverture a échoué
		printf("\nfile loading failed\n");
		return 0;
	}
	FILE* gnuplotPipe = popen("gnuplot", "w"); // ou ouvre l'application gnuplot en mode écriture pour "envoyer" des commandes dans cette dernière
	if(gnuplotPipe == NULL){ // gestion de l'erreur si l'ouverture a échoué
		printf("\ngnuplot loading failed\n");
		return 0;
	}

	fprintf(file, "# x (valeur luminance) y (occurence)\n"); // on met un commentaire dans le fichier data
	for(int i=0 ; i<size ; i++){
		fprintf(file, "%d %d\n", i, hist[i]); // on y met toutes les données de l'histogramme
	}

	FILE* histPNG = fopen("./histogram/hist.png", "r"); // on essaie d'ouvrir le fichier png pour savoir s'il existe déjà
	if(histPNG != NULL){ // si on a réussi (s'il existe)
		fclose(histPNG);
		remove("./histogram/hist.png"); // alors on le supprime pour que gnuplot en recrée un nouveau à jour
	}

	char* gnuplotCommands[] = {"set title \"Histogramme de luminance\"", "set xlabel \"Valeur de luminance\"", "set ylabel \"Nb occurences\"", "set autoscale", "set terminal png", "set output \"./histogram/hist.png\"", "plot \"./histogram/hist.dat\" u 1:2 with impulses linewidth 5 linecolor rgb \"black\""}; // un tableau statique de chaînes de caractères, 1 chaîne = 1 commande
	for(int i=0 ; i<7; i++){
		fprintf(gnuplotPipe, "%s\n", gnuplotCommands[i]); // on "envoie" au fur et à mesure les commandes dans l'ordre
	}

	fclose(gnuplotPipe); // on n'oublie pas de "fermer l'application"
	fclose(file); // on ferme aussi le fichier data
	return 1; // tout s'est bien passé
}

void dezoom(IMAGE* img){
	PIXEL** newTabPixels = createTabPixels(img->height/2, img->width/2); // on crée le nouveau tableau de pixels avec la nouvelle taille
	int safeNumberLine = 0; // variable pour gérer le dépacement de line
	int safeNumberColumn = 0; // variable pour gérer le dépacement de column

	for (int line = 0; line < img->height; line++){ // on parcours les pixels de l'image de base
		for (int column = 0; column < img->width; column++){
			if ((line+1) % 2 == 0 && (column+1) % 2 == 0){ // si le pixel en bas à droite de carré de pixel 2x2 dont le pixel d'origine est celui en haut à gauche, est un multiple de 2 (la ligne et la colonne)
				if((line+1)==img->height){ // si on dépasse d'une ligne, on ne la prendra pas en compte
					safeNumberLine=1;
				}
				if((column+1)==img->width){ // si on dépasse d'une colonne, on ne la prendra pas en compte
					safeNumberColumn=1;
				}

				/* on fait la moyenne du carré de 4 pixel pour les 4 canaux */
				newTabPixels[line/2][column/2].red=(img->tabPixels[line+1-safeNumberLine][column+1-safeNumberColumn].red + img->tabPixels[line+1-safeNumberLine][column].red + img->tabPixels[line][column+1-safeNumberColumn].red+img->tabPixels[line-1][column-1].red)/4;
				newTabPixels[line/2][column/2].green=(img->tabPixels[line+1-safeNumberLine][column+1-safeNumberColumn].green + img->tabPixels[line+1-safeNumberLine][column].green + img->tabPixels[line][column+1-safeNumberColumn].green+img->tabPixels[line-1][column-1].green)/4;
				newTabPixels[line/2][column/2].blue=(img->tabPixels[line+1-safeNumberLine][column+1-safeNumberColumn].blue + img->tabPixels[line+1-safeNumberLine][column].blue + img->tabPixels[line][column+1-safeNumberColumn].blue+img->tabPixels[line-1][column-1].blue)/4;
			}
		}
	}

	freeTabPixels(img->tabPixels, img->height);
	img->width=img->width/2;
	img->height=img->height/2;
	img->tabPixels=newTabPixels;
}

void zoom(IMAGE* img){
	PIXEL** newTabPixels = createTabPixels(img->height*2, img->width*2); // on crée le nouveau tableau de pixels avec la nouvelle taille

	for (int line = 0; line < img->height; line++){ // on parcours les pixels de l'image de base
		for (int column = 0; column < img->width; column++){

			/* 1 pixel de l'ancienne image = 4 pixels de la nouvelle */ 
			newTabPixels[line*2][column*2].red=img->tabPixels[line][column].red;
			newTabPixels[line*2][column*2].green=img->tabPixels[line][column].green;
			newTabPixels[line*2][column*2].blue=img->tabPixels[line][column].blue;

			newTabPixels[line*2][column*2+1].red=img->tabPixels[line][column].red;
			newTabPixels[line*2][column*2+1].green=img->tabPixels[line][column].green;
			newTabPixels[line*2][column*2+1].blue=img->tabPixels[line][column].blue;

			newTabPixels[line*2+1][column*2+1].red=img->tabPixels[line][column].red;
			newTabPixels[line*2+1][column*2+1].green=img->tabPixels[line][column].green;
			newTabPixels[line*2+1][column*2+1].blue=img->tabPixels[line][column].blue;

			newTabPixels[line*2+1][column*2].red=img->tabPixels[line][column].red;
			newTabPixels[line*2+1][column*2].green=img->tabPixels[line][column].green;
			newTabPixels[line*2+1][column*2].blue=img->tabPixels[line][column].blue;
		}
	}
	
	freeTabPixels(img->tabPixels, img->height);
	img->width=img->width*2;
	img->height=img->height*2;
	img->tabPixels=newTabPixels;
}