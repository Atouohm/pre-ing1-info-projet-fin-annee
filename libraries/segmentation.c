#include <segmentation.h>

/*
	Auteur : Matheo BIOUDI
	Date : 28/05/2022
	Résumé : 2 macros pour déterminer le minimum ou le maximum entre 2 nombres
*/
#define MIN(a,b) (a<b?a:b)
#define MAX(a,b) (a>b?a:b)

FILTER* createCrossFilter(int size){
	FILTER* crossFilter = malloc(sizeof(FILTER)); // création et allocation du pointeur de structure qui sera retourné à la fin de la fonction
	crossFilter->size = size; // on lui donne sa taille

	/* allocation du tableau dynamique tabFilter de la structure croix */
	crossFilter->tabFilter = malloc((crossFilter->size)*sizeof(int*)); // lignes
	for(int line=0 ; line<size ; line++){
		crossFilter->tabFilter[line] = malloc((crossFilter->size)*sizeof(int)); // puis colonnes
	}

	int indexCross=size/2; // numéro de la ligne et de la colonne où la croix doit être créée dans le tableau 2D de la structure filtre (c une div e ici)
		
	// on remplit le filtre
	for(int line=0 ; line<size ; line++){
		for(int colonne=0 ; colonne<size ; colonne++){
			if(colonne == indexCross || line == indexCross){crossFilter->tabFilter[line][colonne] = 1;} else {crossFilter->tabFilter[line][colonne] = 0;}
		}
	}

	return crossFilter; // on le retourne
}

void imgErode(IMAGE* img, FILTER* filter){
	int min; // on déclare la variable min qui stockera consécutivement les minimums pour éroder
	int origin = filter->size/2; // permet de se placer à l'origine de l'élément structurant (de la croix), ie son centre
	PIXEL** newTabPixels = createTabPixels(img->height, img->width); // on crée le nouveau tableau de pixel qui sera celui érodé

	for(int line = 0 ; line < img->height; line++){ // on parcours toutes les lignes de l'image
		for (int column = 0; column < img->width; column++){ // on parcours toutes les colonnes de l'image

			min = img->colorRange; // minimum défini par défaut au maximum de la color range
			for(int cooX=0 ; cooX<filter->size ; cooX++){ // on parcours toutes les lignes du filtre d'érosion (= l'élément structurant)
				for(int cooY=0 ; cooY<filter->size ; cooY++){ // on parcours toutes les colonnes du filtre d'érosion (= l'élément structurant)
					if(line-origin+cooX >= 0 && line-origin+cooX < img->height && column-origin+cooY >= 0 && column-origin+cooY < img->width && filter->tabFilter[cooX][cooY]==1){ // on vérifie que l'on ne dépasse pas l'image, et on regarde si le filtre nous indique ou non de prendre en compte le pixel
						min = MIN(min, img->tabPixels[line-origin+cooX][column-origin+cooY].red); // si tout va bien, on prend le minimum entre l'ancien minimum et le pixel sur lequel on est
					}
				}
			}

			/* on affecte le minimum trouvé de tous les pixels concernés par le filtre aux 3 canaux de couleur */
			newTabPixels[line][column].red = min;
			newTabPixels[line][column].green = min;
			newTabPixels[line][column].blue = min;
		}
	}

	freeTabPixels(img->tabPixels, img->height); // on libère l'ancien tableau de pixels de l'image qui était celui de base, non érodé
	img->tabPixels = newTabPixels; // on lui donne le nouveau
}

void imgDilate(IMAGE* img, FILTER* filter){
	int max; // on déclare la variable max qui stockera consécutivement les minimums pour éroder
	int origin = filter->size/2; // permet de se placer à l'origine de l'élément structurant (de la croix), ie son centre
	PIXEL** newTabPixels = createTabPixels(img->height, img->width); // on crée le nouveau tableau de pixel qui sera celui érodé

	for(int line = 0 ; line < img->height; line++){ // on parcours toutes les lignes de l'image
		for (int column = 0; column < img->width; column++){ // on parcours toutes les colonnes de l'image

			max = 0; // maximum défini par défaut au minimum de la color range (0 dans tous les cas)
			for(int cooX=0 ; cooX<filter->size ; cooX++){ // on parcours toutes les lignes du filtre d'érosion (= l'élément structurant)
				for(int cooY=0 ; cooY<filter->size ; cooY++){ // on parcours toutes les colonnes du filtre d'érosion (= l'élément structurant)
					if(line-origin+cooX >= 0 && line-origin+cooX < img->height && column-origin+cooY >= 0 && column-origin+cooY < img->width && filter->tabFilter[cooX][cooY]==1){ // on vérifie que l'on ne dépasse pas l'image, et on regarde si le filtre nous indique ou non de prendre en compte le pixel
						max = MAX(max, img->tabPixels[line-origin+cooX][column-origin+cooY].red); // si tout va bien, on prend le maximum entre l'ancien maximum et le pixel sur lequel on est
					}
				}
			}

			/* on affecte le maximum trouvé de tous les pixels concernés par le filtre aux 3 canaux de couleur */
			newTabPixels[line][column].red = max;
			newTabPixels[line][column].green = max;
			newTabPixels[line][column].blue = max;
		}
	}

	freeTabPixels(img->tabPixels, img->height); // on libère l'ancien tableau de pixels de l'image qui était celui de base, non érodé
	img->tabPixels = newTabPixels; // on lui donne le nouveau
}