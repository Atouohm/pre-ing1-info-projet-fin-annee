PRE-ING1 Projet 2021/2022, groupe "ALTF4"

Membres :
Julien GITTON (G1)
Matheo BIOUDI (G1)
Dorian BARRERE (G1)

Les dépendances :
- stdio.h
- stdlib.h
- string.h
- time.h
- math.h

Instructions :
1) La règle du makefile pour créer l'exécutable "./img" est "img", cela donne "make img"
2) L’exécutable est ./img
3) Pour supprimer tous les fichiers objects, utiliser "make clean"
4) Utiliser "./img -h" pour avoir l'aide
5) L’utilisation erronée de commandes génère une erreur et affiche l’aide
6) En général (sauf si "-x") si l’option "-o" n’est pas utilisée, alors l’image générée sera affichée dans la console
7) L’option "-x" génère d’office un fichier ./images/croix.ppm si l’option -o n’est pas utilisée

Les outils de base :
- "-h" (à utiliser sans rien d’autre) aide, si ./img est exécutée tout seul l’aide va également être affichée mais en tant qu’erreur
- "-i" <fichier ppm> fichier d'entrée (obligatoire sauf si -h OU -x où -i interdit)
- "-o" <fichier> fichier de sortie
- "-x" <largeur> <hauteur> <épaisseur> croix

Les options de transformation :
- "-a" histogramme graphique généré dans ./histogram/hist.png, le fichier ./histogram/dat.png contient les données
- "-p" rotation de 90 degrés vers la droite
- "-m" miroir
- "-g" mise en niveaux de gris
- "-b" <seuil> binarisation
- "-n" négatif
- "-r" recadrage dynamique
- "-c" renforcement de contraste
- "-f" flou
- "-I" contours
- "-e" érosion
- "-d" dilatation
- "-z" dezoom (= rétrécissement de l'image)
- "-Z" zoom (= agrandissement de l'image)