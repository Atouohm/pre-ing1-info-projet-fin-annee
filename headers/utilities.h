#ifndef UTILITIES_H
#define UTILITIES_H

/*
	Auteur : Dorian BARRERE
	Date : 21/05/2022
	Résumé : structure représentant un pixel
*/
typedef struct pixel{
	int red;
	int green;
	int blue;
} PIXEL;

/*
	Auteur : Dorian BARRERE
	Date : 21/05/2022
	Résumé : structure représentant une image
*/
typedef struct image{
	int width;
	int height;
	int colorRange;
	PIXEL** tabPixels;
} IMAGE;

/* inclusion des headers des bibliothèques classiques */
#include "basicLibraries.h"

/* un include suplémentaire nécessaires à cette bibliothèque, il est important de le mettre APRES la déclaration de nouveau types structurés */
#include "segmentation.h"
#include "autoParsing.h"

/*
	Auteur : Dorian BARRERE
	Date : 21/05/2022
	Résumé : procédure qui stoppe le programme en affichant avant le message voulu, en mode erreur ou simplement succès
	Entrée(s) : une chaîne de caractères (le message, ne rien mettre si pas de message souhaité), une entier 0 pour succès ou 1 (ou tout autre entier) pour une erreur
	Sortie(s) : rien
*/
void stopProg(const char* message, int why);

/*
	Auteur : Dorian BARRERE
	Date : 27/05/2022
	Résumé : fonction qui crée un tableau de pixels dynamique en 2D avec la taille souhaitée
	Entrée(s) : un entier pour le nombre de lignes, et un autre entier pour le nombre de colonnes
	Sortie(s) : le tableau de pixel dynamique (l'adresse)
*/
PIXEL** createTabPixels(int nbLines, int nbColumns);

/*
	Auteur : Dorian BARRERE
	Date : 27/05/2022
	Résumé : procédure qui libère automatique une tableau de pixel
	Entrée(s) : l'adresse du tableau de pixel en 2D, et le nombre de lignes du tableau
	Sortie(s) : rien
*/
void freeTabPixels(PIXEL** tabPixels, int nbLines);

/*
	Auteur : Dorian BARRERE
	Date : 24/05/2022
	Résumé : procédure qui libère automatique une image créée avec la fonction createImage()
	Entrée(s) : l'adresse d'une IMAGE (type structuré)
	Sortie(s) : rien
*/
void freeImage(IMAGE* img);

/*
	Auteur : Dorian BARRERE
	Date : 04/06/2022
	Résumé : procédure qui libère automatique un filtre carré 2D généré pour la dilatation ou l'érosion
	Entrée(s) : l'adresse du filtre (type structuré)
	Sortie(s) : rien
*/
void freeFilter(FILTER* filter);

/*
	Auteur : Dorian BARRERE
	Date : 21/05/2022
	Résumé : fonction qui permet de créer une structure d'image allouée aux valeurs souhaitées
	Entrée(s) : 3 entiers représentants la largeur, la hauteur, et l'échelle de couleur de l'image à créer
	Sortie(s) : l'adresse de l'image créée
*/
IMAGE* createImage(int width, int height, int colorRange);

/*
	Auteur : Dorian BARRERE
	Date : 21/05/2022
	Résumé : fonction qui sauvegarde les données d'une structure image dans un fichier choisi (le crée s'il n'existe pas)
	Entrée(s) : une structure image, et une chaîne de caractères (le chemin de l'image)
	Sortie(s) : un entier, 0 si sauvegarde échouée, 1 si ok
*/
int saveImage(IMAGE img, const char* target);

/*
	Auteur : Dorian BARRERE
	Date : 04/06/2022
	Résumé : procédure affiche une image ppm dans la console
	Entrée(s) : une structure image
	Sortie(s) : rien
*/
void printImage(IMAGE img);

/*
	Auteur : Dorian BARRERE
	Date : 21/05/2022
	Résumé : fonction qui charge une image et renvoie un pointeur vers la structure image chargée
	Entrée(s) : une chaîne de caractères (le chemin de l'image)
	Sortie(s) : l'adresse de l'image chargée
*/
IMAGE* loadImage(const char* source);

/*
	Auteur : Dorian BARRERE
	Date : 21/05/2022
	Résumé : procédure qui crée une image en croix dont on choisit l'épaisseur, si le fichier image n'existe pas il est créé, s'il existe, il est écrasé
	Entrée(s) : une chaîne de caractères (le chemin de l'image), un entier pour la largeur, la hauteur, et un entier non signé représentant l'épaisseur de la croix
	Sortie(s) : rien
*/
void customCross(FILE* targetFile, int width, int height, unsigned int thickness);

/*
	Auteur : Julien Gitton
	Date : dim. 22 mai 2022 15:55:59 UTC
	Résumé : réalise l'histogramme d'une image en niveux de gris
	Entrée(s) : une IMAGE (type structuré) contenant les informations d'une image en nuance de gris
	Sortie(s) : un tableau contenant la fréquence d'apparition de chaque niveau de gris
*/
int* imgHist(IMAGE img);

/*
	Auteur : Dorian BARRERE
	Date : 24/05/2022
	Résumé : fonction qui ramène un nombre supérieur à 255 à 255, et un nombre inférieur à 0 à 0
	Entrée(s) : un flottant, un entier pour la colorRange de l'image à respecter
	Sortie(s) : un entier
*/
int keepColorRange(float value, int colorRange);

/* 
	Auteur : Julien Gitton
	Date :  mer. 01 juin 2022 18:19:46 UTC
	Résumé :  affiche à l'écran de l'utilisateur une aide pour l'utilisation de la ligne de commande
	Entrée(s) : rien
	Sorties(s) : affichage de l'aide
*/
void printHelp();

/*
	Auteur : Dorian BARRERE
	Date : 03/05/2022
	Résumé : procédure qui automatise la fin de la fonction main, prématurée ou non
	Entrée(s) : un message (ou laisser vide pour ne pas en mettre), 0 ou 1 pour succès ou erreur, les adresses successives de l'image, du filtre, de la liste d'instruction, et au final les 2 chaînes de caractères correspondant au fichier cible et source
	Sortie(s) : rien
*/
void endOfMain(const char* message, int why, IMAGE* img, FILTER* crossFilter, LISTINSTRUCTIONS* listInstructions, char* target, char* source);

#endif