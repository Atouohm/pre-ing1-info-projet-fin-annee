#ifndef BONUS_H
#define BONUS_H

/* inclusion des headers des bibliothèques classiques */
#include "basicLibraries.h"

/* un include suplémentaire nécessaires à cette bibliothèque */
#include "utilities.h"

/*
	Auteur : Dorian BARRERE
	Date : 02/06/2022
	Résumé : procédure qui sauvegarde un histogramme dans un fichier png (./histogram/hist.png) avec l'application gnuplot, en générant au passage un fichier data  (./histogram/hist.dat) correspondant aux données de l'histogramme sous forme de 2 colonnes
	Entrée(s) : le tableau d'entiers, et la taille du tableau c'est à dire le colorRange+1 de l'image actuellement étudiée
	Sortie(s) : un entier, 0 si échouée, 1 si ok
*/
int saveHistogram(int* hist, int size);

/*
	Auteur : Matheo BIOUDI
	Date : 04/06/2022
	Résumé : procédure dézoome une image (rétrécie pour être précis)
	Entrée(s) : l'adresse d'une image (type structuré)
	Sortie(s) : rien
*/
void dezoom(IMAGE* img);

/*
	Auteur : Matheo BIOUDI
	Date : 04/06/2022
	Résumé : procédure zoome une image (l'agrandie pour être précis)
	Entrée(s) : l'adresse d'une image (type structuré)
	Sortie(s) : rien
*/
void zoom(IMAGE* img);

#endif
