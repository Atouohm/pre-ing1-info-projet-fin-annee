#ifndef SEGMENTATION_H
#define SEGMENTATION_H

/*
	Auteur : Matheo BIOUDI
	Date : 28/05/2022
	Résumé : structure représentant un filtre pour l'érosion et la dilatation
*/
typedef struct filter{
	int size;
	int** tabFilter;
} FILTER;

/* inclusion des headers des bibliothèques classiques */
#include "basicLibraries.h"

/* un include suplémentaire nécessaires à cette bibliothèque */
#include "utilities.h"

/*
	Auteur : Matheo BIOUDI et Dorian BARRERE
	Date : 28/05/2022
	Résumé : fonction crée un filtre (= un élément structurant) en croix pour l'érosion et la dilatation
	Entrée(s) : un entier pour la taille (= longeur = largeur de la croix, PAS le nombre de 1 formant la croix)
	Sortie(s) : l'adresse du filtre créé
*/
FILTER* createCrossFilter(int size);

/*
	Auteur : Matheo BIOUDI et Dorian BARRERE
	Date : 28/05/2022
	Résumé : procédure qui érode une image
	Entrée(s) : l'adresse d'une IMAGE (type structuré) en niveaux de gris OU binaire, et l'adresse du filtre (type structuré) que l'on veut utilisé
	Sortie(s) : rien
*/
void imgErode(IMAGE* img, FILTER* filter);

/*
	Auteur : Matheo BIOUDI et Dorian BARRERE
	Date : 28/05/2022
	Résumé : procédure qui dilate une image
	Entrée(s) : l'adresse d'une IMAGE (type structuré) en niveaux de gris OU binaire, et l'adresse du filtre (type structuré) que l'on veut utilisé
	Sortie(s) : rien
*/
void imgDilate(IMAGE* img, FILTER* filter);

#endif