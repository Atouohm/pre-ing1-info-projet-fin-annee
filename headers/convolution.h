#ifndef CONVOLUTION_H
#define CONVOLUTION_H

/* inclusion des headers des bibliothèques classiques */
#include "basicLibraries.h"

/* un include suplémentaire nécessaires à cette bibliothèque */
#include "utilities.h"

/*
	Auteur : Dorian BARRERE
	Date : 24/05/2022
	Résumé : fonction qui applique à une image une matrice de convolution 3x3 (elle est normalisée automatiquement si ne l'est pas)
	Entrée(s) : l'adresse de la structure file (du fichier), et une matrice 3x3 de nombres flottants
	Sortie(s) : rien
*/
void applyConvolution(IMAGE* img, float matrix[3][3]);

/*
	Auteur : Dorian BARRERE
	Date : 24/05/2022
	Résumé : fonction qui applique à une image la matrice de convolution 3x3 qui ajoute du contraste
	Entrée(s) : l'adresse de la structure file (du fichier)
	Sortie(s) : rien
*/
void applyContrast(IMAGE* img);

/*
	Auteur : Dorian BARRERE
	Date : 24/05/2022
	Résumé : fonction qui applique à une image la matrice de convolution 3x3 qui floute
	Entrée(s) : l'adresse de la structure file (du fichier)
	Sortie(s) : rien
*/
void applyBlurring(IMAGE* img);

/*
	Auteur : Dorian BARRERE
	Date : 24/05/2022
	Résumé : fonction qui applique à une image la matrice de convolution 3x3 qui faire apparaître les contours
	Entrée(s) : l'adresse de la structure file (du fichier)
	Sortie(s) : rien
*/
void applyContours(IMAGE* img);

#endif