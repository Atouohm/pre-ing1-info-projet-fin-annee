#ifndef BASIC_HANDLING_H
#define BASIC_HANDLING_H

/* inclusion des headers des bibliothèques classiques */
#include "basicLibraries.h"

/* un include suplémentaire nécessaires à cette bibliothèque */
#include "utilities.h"

/*
	Auteur : Julien Gitton
	Date : dim. 22 mai 2022 11:25:58 UTC
	Résumé : transforme une image en niveau de gris
	Entrée(s) : l'adresse d'une structure IMAGE contenant l'image en couleur
	Sortie(s) : rien
*/
void imgGreyTransform(IMAGE* img);

/*
	Auteur : Julien Gitton
	Date : dim. 22 mai 2022 14:15:54 UTC
	Résumé : Binarisation d'une image
	Entrée(s) : l'adresse d'une IMAGE (type structuré) contenant toutes les informations de l'image à binariser ainsi que la valeur de seuil inférieure au color range
	Sortie(s) : rien
*/
void imgBinar(IMAGE* img, int seuil);

/*
	Auteur : Julien Gitton
	Date : dim. 22 mai 2022 14:40:26 UTC
	Résumé : réalise le miroir d'une image
	Entrée(s) : l'adresse d'une IMAGE (type structuré) contenant toutes les informations de l'image
	Sortie(s) : rien
*/
void imgMirror(IMAGE* img);

/*
	Auteur : Julien Gitton puis Dorian BARRERE le 30/05/2022
	Date : dim. 22 mai 2022 15:00:34 UTC
	Résumé : rotation d'une image de 90° dans le sens horaire
	Entrée(s) : l'adresse d'une IMAGE (type structuré) contenant toutes les informations de l'image
	Sortie(s) : rien
*/
void imgRotate(IMAGE* img);

/*
	Auteur : Julien Gitton
	Date : dim. 22 mai 2022 15:37:25 UTC
	Résumé : réalise le négatif d'une image par rapport à sa valeur de luminance maximum
	Entrée(s) : l'adresse d'une IMAGE (type structuré) contenant toutes les informations de l'image
	Sortie(s) : rien
*/
void imgNegative(IMAGE* img);

/*
	Auteur : Dorian BARRERE
	Date : 27/05/2022
	Résumé : procédure qui effectue un recadrage dynamique de sur une image en niveaux de gris
	Entrée(s) : l'adresse d'une IMAGE (type structuré) contenant toutes les informations de l'image
	Sortie(s) : rien
*/
void dynamicCropping(IMAGE* img);

#endif
