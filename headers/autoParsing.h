#ifndef AUTOPARSING_H
#define AUTOPARSING_H

/*
	Auteur : Dorian BARRERE
	Date : 03/05/2022
	Résumé : type structuré représentant une liste d'instructions ordonnées sous forme de tableau de chaîne de caractères
*/
typedef struct listIntructions{
	int size;
	const char** list; // le contenu de chaque chaîne de la liste sera constante et donc non modifiable, mais le pointeur en lui-même n'est pas constant donc on peut changer la chaîne en pointant vers une autre, de plus si on récupère l'adresse d'une chaîne de tableau à part dans un pointeur de char à part, alors on pourra modifier le contenu de la chaîne, ce qui modifiera aussi la même chaîne (principe de pointeur) dans la liste alors qu'elle est constante (ie qu'on ne peut pas le faire directement depuis la liste, mais c possible en récupérant l'adresse et en le faisant "à coté")
} LISTINSTRUCTIONS;

/* inclusion des headers des bibliothèques classiques */
#include "basicLibraries.h"

/* un include suplémentaire nécessaires à cette bibliothèque */
#include "utilities.h"

/*
	Auteur : Dorian BARRERE
	Date : 03/05/2022
	Résumé : procédure qui automatise la fin prématurée de la fonction getInstructions()
	Entrée(s) : l'adresse de la structure listInstructions, l'adresse des 6 chaînes de caractères (l'ordre importe en réalité peu)
	Sortie(s) : rien
*/
void endOfGetInstructions(LISTINSTRUCTIONS* listInstructions, char* iValue, char* oValue, char* xValue, char* yValue, char* thicknessValue, char* bValue);

/*
	Auteur : Dorian BARRERE
	Date : 03/05/2022
	Résumé : fonction qui retourne l'adresse structure listInstructions contenant les instructions ordonnées (suivant toutes les règles du sujet) à effectuer dans l'ordre, cela en mettant simplement en paramètre le argv du main
	Entrée(s) : le argc et le argv du main
	Sortie(s) : l'adresse de la structure listInstructions
*/
LISTINSTRUCTIONS* getInstructions(int argc, char* const argv[]);

#endif
